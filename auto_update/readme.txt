This folder contains firmware updates which are downloaded by the automatic firmware update check of the ProVideo GUI.

To add a firmware update follow this procedure:

1. If not existing, create a folder for the device which is named like the device platform id (like xbow, cooper or condor4k)
2. Copy the latest firmware update to that folder
3. In that folder create the file "update_info.txt", this file is used by the GUI to find out which files to download
   and to get their checksum. Below is an example for the content of update_info.txt for a cooper device:
   
version: V1_0_2
file0: ATOM_one_mini_V1_0_2_bitstream.bin 858e483829484dbf6c23b790ed6e3105
file1: ATOM_one_mini_V1_0_2_sw.bin 81ecc2570fbcc1a54a0066351f666284

4. The version must match the software id of the update file, file0 and file1 specify the software and bitstream update files.
   For devices with only one upate file (e.g. xbow) file1 is omitted.
5. After the filename comes the MD5 checksum of the file. Under linux you can caluclate it using the following command:

# md5sum ATOM_one_mini_V1_0_2_sw.bin 
# 81ecc2570fbcc1a54a0066351f666284  ATOM_one_mini_V1_0_2_sw.bin

For the ProVideo GUI only the latest Version number has to be provided at ./provideo_gui/update_info.txt, the file needs to have
the following content:

version: V1_1_0
